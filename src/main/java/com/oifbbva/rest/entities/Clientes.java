package com.oifbbva.rest.entities;

import java.util.List;

public class Clientes {
    private String idClient;
    private String numContract;
    private List<Productos> products;
    private String emailAddress;
    private String password;

    public Clientes(String idClient, String numContract, List<Productos> products, String emailAddress, String password) {
        this.idClient = idClient;
        this.numContract = numContract;
        this.products = products;
        this.emailAddress = emailAddress;
        this.password = password;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getNumContract() {
        return numContract;
    }

    public void setNumContract(String numContract) {
        this.numContract = numContract;
    }

    public List<Productos> getProducts() {
        return products;
    }

    public void setProducts(List<Productos> products) {
        this.products = products;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
