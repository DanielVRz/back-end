package com.oifbbva.rest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.oifbbva.rest.entities.Productos;
import com.oifbbva.rest.repositories.ProductosRepository;


@Service
public class ProductosService {
	
	@Autowired
	private ProductosRepository productoRepository;

	public List<Productos> getProductos() {
		return productoRepository.findAll();
	}

	public Productos getProducto(String nombre) {
		for(Productos producto: productoRepository.findAll() ) {
			if(producto.getNombre().equals(nombre)) {
				return producto;
			}
		}
		return null;
	}

}
