package com.oifbbva.rest.entities;

import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "productos")
public class Productos {
    @NonNull
    private String nombre;
    private List<String> plazo;
    private Double montoMinimo;
    private Double montoMaximo;
    private Double tasaBruta;

    public Productos() {
    }

    public Productos(@NonNull String nombre, List<String> plazo, Double montoMinimo, Double montoMaximo, Double tasaBruta) {
        this.nombre = nombre;
        this.plazo = plazo;
        this.montoMinimo = montoMinimo;
        this.montoMaximo = montoMaximo;
        this.tasaBruta = tasaBruta;
    }

    @NonNull
    public String getNombre() {
        return nombre;
    }

    public void setNombre(@NonNull String nombre) {
        this.nombre = nombre;
    }

    public List<String> getPlazo() {
        return plazo;
    }

    public void setPlazo(List<String> plazo) {
        this.plazo = plazo;
    }

    public Double getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(Double montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public Double getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(Double montoMaximo) {
        this.montoMaximo = montoMaximo;
    }

    public Double getTasaBruta() {
        return tasaBruta;
    }

    public void setTasaBruta(Double tasaBruta) {
        this.tasaBruta = tasaBruta;
    }
}
