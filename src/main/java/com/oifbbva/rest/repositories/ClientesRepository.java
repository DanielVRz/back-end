package com.oifbbva.rest.repositories;

import com.oifbbva.rest.entities.Productos;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesRepository extends MongoRepository<Productos, String> {

}
