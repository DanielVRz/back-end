package com.oifbbva.rest.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.oifbbva.rest.entities.Productos;

import java.util.Optional;

@Repository
public interface ProductosRepository extends MongoRepository<Productos, String>{

}
