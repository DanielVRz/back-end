package com.oifbbva.rest.controllers;

import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import com.oifbbva.rest.entities.Calculo;
import com.oifbbva.rest.entities.ProductoCalculo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.oifbbva.rest.entities.Productos;
import com.oifbbva.rest.services.ProductosService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path ="/productos")
public class ProductosController {
	
	
	@Autowired
	private ProductosService productosService;

	@GetMapping("/obtenerProductos")
	public List<Productos> getProductos(){
		return productosService.getProductos();
	}
 
	@GetMapping("/{nombre}")
	public ResponseEntity<Productos> getProducto(@PathVariable("nombre") String nombre){
		Productos producto = productosService.getProducto(nombre.toUpperCase(Locale.ROOT));
		if(producto == null) {
			return ResponseEntity.notFound().build();
		} else {
			System.out.println(producto);
			return ResponseEntity.ok().body(producto);
		}
	}

	@PostMapping("/calculo/{nombre}")
	public ResponseEntity<Calculo> getCalculo(@PathVariable("nombre") String nombre, @RequestBody ProductoCalculo productoCalculo){
		Double cantidad = Double.valueOf(productoCalculo.getValor().trim().replace(",", ""));
		Productos producto = productosService.getProducto(nombre.toUpperCase(Locale.ROOT));
		Calculo calculo = new Calculo();
		calculo.setInversion(cantidad);
		calculo.setInteres(
				producto.getTasaBruta() * cantidad * Double.valueOf(productoCalculo.getPlazo().trim().replaceAll("[A-z]+", ""))
		);
		calculo.setIsr(
				cantidad * 23.52
		);
		calculo.setTitulos(100);
		calculo.setTotal(cantidad + calculo.getInteres() - calculo.getIsr());
		return ResponseEntity.ok().body(calculo);
	}
}
