package com.oifbbva.rest.entities;

public class Data {
    private Productos productos;

    public Data() {
    }

    public Data(Productos productos) {
        this.productos = productos;
    }

    public Productos getProductos() {
        return productos;
    }

    public void setProductos(Productos productos) {
        this.productos = productos;
    }
}
