package com.oifbbva.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OifbbvaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OifbbvaApplication.class, args);
	}

}
